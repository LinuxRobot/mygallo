#!/bin/bash

VALUE='0000'
PREFIX='JLMR'

function loadEnviroment {
	USER_ID='61c37893-e229-4360-9246-ee3b0e43b719'

	BORN_DATE_FORMAT="`date +%Y`-$((1 + RANDOM % 12))-$((1 + RANDOM % 30)) 00:00:00"

	# NULL AMARILLO NARANGA ROJO MORADO AZUL VERDE NEGRO PLATEADO ROSA
	TIPO_PLACA="$((1 + RANDOM % 10))"
	MARCA="$((2 + RANDOM % 44))"
	SEXO="`if [ $((1 + RANDOM % 2)) -eq 1 ]; then echo 'M'; else echo 'H'; fi`"
	ESTADO=1
	ENRACE=1
	COMMENT=''
	COLOR="$((1 + RANDOM % 14))"
	CRESTA="$((1 + RANDOM % 5))"
}

#generate the next gallos id by current value
function next_gallo_identity { 
	VALUE=`expr $VALUE + 1`
	length=`expr 4 - ${#VALUE}`
	while [ $length -gt 0 ]; do
	       VALUE="0"$VALUE
       		length=`expr $length - 1`
 	done	
}

#function noPlacayTipo {
#	case $TIPO_PLACA in
#	1)
#		AMARILLO=`expr $AMARILLO + 1`
#		;;
#	2)
#		NARANJA=`expr $NARANJA + 1`
#		;;
#	3)
#		ROJO=`expr $ROJO + 1`
#		;;
#	4)
#		MORADO=`expr $MORADO + 1`
#		;;
#	5)
#		AZUL=`expr $AZUL + 1`
#		;;
#	6)
#		VERDE=`expr $VERDE + 1`
#		;;
#	7)
#		NEGRO=`expr $NEGRO + 1`
#		;;
#	8)
#		PLATEADO=`expr $PLATEADO + 1`
#		;;
#	9)
#		ROSA=`expr $ROSA + 1`
#		;;
#	esac	
#} 

i=0
echo "insert into gallos values "
while [ $i -lt $1 ]; do
	i=`expr $i + 1`
	next_gallo_identity
	loadEnviroment
	echo "('$PREFIX$VALUE','$BORN_DATE_FORMAT','',$i,$MARCA,null,'$USER_ID',null,null,'$SEXO',$ESTADO,$ENRACE,'',$COLOR,$CRESTA,$TIPO_PLACA),"
done
