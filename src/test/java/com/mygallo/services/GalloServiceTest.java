package com.mygallo.services;

import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static com.mygallo.ConfigurationTest.*;
import com.mygallo.entities.Gallos;

import java.math.BigInteger;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GalloServiceTest {

	private Gallos g;
	@Autowired
	private GalloService galloService;

	
	@Test
	@Ignore
	public void shouldBeReturnGalloFromDB() {
		g = galloService.findGalloByUserAndId(USERID, GALLOID);
		assertNotNull(g);
	}

	@Test
	@Ignore
	public void shouldBeDeleteGalloWithChild() {
		galloService.removeGallo(GALLO);
	}

	@Test
	public void shouldBeReturnBornsPerMonth(){
        int result[] = galloService.groupBornsByMonth(USERID,"M");
		assertNotNull(result);
        for(int i:result) {
            System.out.println(i);
        }
    }
}
