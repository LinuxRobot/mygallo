package com.mygallo.controllers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.mygallo.controllers.EditGalloController;
import com.mygallo.entities.Gallos;
import com.mygallo.entities.Usuarios;
import com.mygallo.repositories.UsuariosRepository;
import static com.mygallo.ConfigurationTest.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EditGalloControllerTest {
	
	@Autowired private EditGalloController editGalloController;
	@Autowired private UsuariosRepository usuarioRepository;
	private Gallos g;
	private Usuarios usuario;
	
	@Before
	public void init() {
		g = new Gallos();
		g.setIdGallo(GALLOID);
		usuario = usuarioRepository.findOne(USERID);
		
	}
	
	@Test(expected=Exception.class)
	public void shouldReturnException() {
		editGalloController.init();
		assertNull(editGalloController.getUsuario());
	}
	
//	@Test()
//	public void shouldReturnNotNullLists() {
//	
//		editGalloController.setGallo(g);
//		
//		editGalloController.setUsuario(usuario);
//		editGalloController.initNew();
//
//		assertNotNull(usuario);
//		assertNotNull(g);
//		assertNotNull(editGalloController.getEstados());
//		assertNotNull(editGalloController.getEnraces());
//		assertNotNull(editGalloController.getGallosHembra());
//		assertNotNull(editGalloController.getGallosMacho());
//		assertNotNull(editGalloController.getMarcas());
//		assertNotNull(editGalloController.getColorAlas());
//		
//		editGalloController.getEstados().forEach((e)->{
//			System.out.println(e.getEstado());
//		});
//		
//		editGalloController.getEnraces().forEach((e)->{
//			System.out.println(e.getEnrace());
//		});
//		
//		editGalloController.getGallosHembra().forEach((g)->{
//			System.out.println(g.getIdGallo());
//		});
//		
//		editGalloController.getGallosMacho().forEach((g)->{
//			System.out.println(g.getIdGallo());
//		});
//		
//		editGalloController.getMarcas().forEach((m)->{
//			System.out.println(m.getMarca());
//		});
//		
//		editGalloController.getColorAlas().forEach((c)->{
//			System.out.println(c.getColor());
//			
//		});
//	}
}
