package com.mygallo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyGalloApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void shuldPrintDateFormated(){
		System.out.println(Calendar.getInstance().get(Calendar.YEAR)+"-"+"09"+"%");
	}

}
