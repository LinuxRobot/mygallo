package com.mygallo.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.mygallo.ConfigurationTest.*;
import com.mygallo.entities.Gallos;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GallosDAOTest {

	@Autowired
	private GallosDAO galloDao;
	
	private Gallos gallo;
	
	@Ignore
	@Test
	public void shouldReturnGalloDAOFromFactory() {
		assertNotNull(galloDao);
	}

	@Ignore
	@Test
	@Transactional
	public void shouldReturnGalloFromDB() {
		gallo = galloDao.findGallosByUserAndId(USERID, GALLOID);
	    assertNotNull(gallo);
	    assertEquals(gallo.getIdGallo(), GALLOID);
	    System.out.println(gallo.getIdGallo());
	    assertNotNull(gallo.getUsuarios());
	    assertNotNull(gallo.getColores());
		assertNotNull(gallo.getTipoCresta());
		assertNotNull(gallo.getEnraces());
		assertNotNull(gallo.getEstados());
		assertNotNull(gallo.getTipoPlaca());
		assertNotNull(gallo.getMarcas());
	}
	
	@Ignore
	@Test
	@Transactional
	public void shouldBeReturnListOfChilds() {
		List<Gallos> childs = galloDao.findChildOfFatherByGallo(GALLO);
		childs.forEach((e)->{
			System.out.println(e.getIdGallo());
		});
		assertNotNull(childs);
	}
	
	@Test
	@Transactional
	public void shouldBeReturnListOfChildsMother() {
		List<Gallos> childs = galloDao.findChildOfMotherByGallo(GALLO);
		childs.forEach((e)->{
			System.out.println(e.getIdGallo());
		});
		assertNotNull(childs);
	}

	@Test
	@Transactional
	public void sholdBeReturnCountGallosByMonthDate(){

		BigInteger value = null;

			value = (BigInteger) galloDao.countBornMonthAndSexByUser(USERID,"09","M");

		System.out.println(value.toString());
		assertNotNull(value);
	}

	@Test
	@Transactional
	public void shouldBeReturnGroupOfMonthBornGallos(){
		List<Map<String,Integer>> result = galloDao.groupCountGalloBornByMonthSexAndUser(USERID,"M");
		assertNotNull(result);
        for (Map m:result) {
            System.out.println(m.get("month"));
            System.out.println(m.get("cant"));
        }
    }
}