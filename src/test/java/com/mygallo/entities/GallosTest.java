package com.mygallo.entities;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.HashSet;

import org.junit.Test;

public class GallosTest {

	private Gallos gallos = new Gallos();
	
	@Test
	public void shouldBeAbleToCreateAGalloWithEmptyConstructor() {
		assertNotNull(gallos);
	}
	
	@Test
	public void shouldBeAbleToSetAGalloId() {
		gallos.setIdGallo("GalloID");
		assertEquals("GalloID", gallos.getIdGallo());
	}
	
	@Test
	public void shoudBeAbleToSetALias() {
		gallos.setAlias("King");
		assertEquals("King", gallos.getAlias());
	}
	
	@Test
	public void shouldBeAbleToCreateAGalloWithParameterConstructor() {
		assertNotNull(new Gallos("id",new Colores(),101,new TipoPlaca(),"King",new Enraces(),new TipoCresta(),new Estados(),new Gallos(),new Gallos(),new Marcas(),new Date(),new Date(),"M"));
	}
	
	@Test
	public void shouldBeAbleToCreateAGalloWithMoreParameterConstructor() {
		assertNotNull(new Gallos("id",new Colores(),101,new TipoPlaca(),"King",new Enraces(),new TipoCresta(),new Estados(),new Gallos(),new Gallos(),new Marcas(),new Date(),new Date(),"M",new Usuarios(),"Hello",new HashSet<>(),new HashSet<>()));
	}
	
	@Test
	public void shouldBeAbleToSetPlaca() {
		gallos.setPlaca(100);
		assertEquals(100,gallos.getPlaca());
	}
	
	@Test
	public void shouldBeAbleToSetTipoPlaca() {
		
		TipoPlaca tipop = new TipoPlaca();
		tipop.setIdTipoPlaca(10);
		
		gallos.setTipoPlaca(tipop);
		assertNotNull(gallos.getTipoPlaca());
		assertEquals(gallos.getTipoPlaca().getIdTipoPlaca(), 10);

	}
	
	@Test
	public void shouldBeAbleToSetEnrace() {
		gallos.setEnraces(new Enraces());
		assertNotNull(gallos.getEnraces());
	}
	
	@Test
	public void shouldBeAbleToSetEstado() {
		gallos.setEstados(new Estados());
		assertNotNull(gallos.getEstados());
		
	}

}
