package com.mygallo.controllers;

import com.mygallo.entities.*;
import com.mygallo.repositories.GallosRepository;
import com.mygallo.services.*;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmorla on 08/07/17.
 */
@Controller()
@Scope("request")
public class GallosController {

    @Autowired private GalloService galloService;
    @Autowired private ColorService colorService;
    @Autowired private TipoCrestaService tipoCrestaService;
    @Autowired private EstadoService estadoService;
    @Autowired private TipoPlacaService tipoPlacaService;

    private List<Colores> colores = new ArrayList<>(0);
    private List<TipoCresta> tipocrestas = new ArrayList<>(0);
    private List<Gallos> gallosList = new ArrayList<>(0);
    private List<Estados> estados = new ArrayList<>(0);
    private List<Gallos> gallosListFiltered;
    private Gallos selectedGallo;
    private Object cantHembras;
    private Object cantMachos;
    private Object cantFallecidos;

    public void init(){
        Usuarios user = (Usuarios) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("user");

        selectedGallo = new Gallos();
        gallosList = galloService.findGallosByUser(user.getIdUsuario());

        colores = colorService.findAllColores();

        tipocrestas = tipoCrestaService.findAllTipoCrestas();

        estados = estadoService.findEstados();

        cantHembras = galloService.countGallosBySexo(user.getIdUsuario(),"H");
        cantMachos = galloService.countGallosBySexo(user.getIdUsuario(),"M");
        cantFallecidos = galloService.countGallosFallecidos(user.getIdUsuario());
    }

    public List<Gallos> getGallosList() {
        return gallosList;
    }

    public void setGallosList(List<Gallos> gallosList) {
        this.gallosList = gallosList;
    }

    public String sexo(String l){
        if(l.equals("M")){
            return "Macho";
        }else if(l.equals("H")){
            return "Hembra";
        }

        return l;
    }
    public void pointGallo(AjaxBehaviorEvent event) {
    	String galloId = (String) event.getComponent().getAttributes().get("galloId");
    	selectedGallo.setIdGallo(galloId);
    }

    public Gallos getSelectedGallo() {
        return selectedGallo;
    }

    public void setSelectedGallo(Gallos selectedGallo) {
        this.selectedGallo = selectedGallo;
    }

    public String updateGallo(String id){
        return "/secure/editGallo?faces-redirect=true&id="+id;
    }

    public List<Colores> getColores() {
        return colores;
    }

    public void setColores(List<Colores> colores) {
        this.colores = colores;
    }

    public List<TipoCresta> getTipocrestas() {
        return tipocrestas;
    }

    public void setTipocrestas(List<TipoCresta> tipocrestas) {
        this.tipocrestas = tipocrestas;
    }

    public List<Estados> getEstados() {
        return estados;
    }

    public void setEstados(List<Estados> estados) {
        this.estados = estados;
    }

    public List<Gallos> getGallosListFiltered() {
        return gallosListFiltered;
    }

    public void setGallosListFiltered(List<Gallos> gallosListFiltered) {
        this.gallosListFiltered = gallosListFiltered;
    }

    public Object getCantHembras() {
        return cantHembras;
    }

    public void setCantHembras(Object cantHembras) {
        this.cantHembras = cantHembras;
    }

    public Object getCantMachos() {
        return cantMachos;
    }

    public void setCantMachos(Object cantMachos) {
        this.cantMachos = cantMachos;
    }

    public Object getCantFallecidos() {
        return cantFallecidos;
    }

    public void setCantFallecidos(Object cantFallecidos) {
        this.cantFallecidos = cantFallecidos;
    }
}
