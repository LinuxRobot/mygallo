package com.mygallo.controllers;

import com.mygallo.common.exceptions.GalloNotFoundException;
import com.mygallo.common.exceptions.InvalidBetweenDateException;
import com.mygallo.common.exceptions.InvalidNoPlacaException;
import com.mygallo.common.exceptions.MyGalloException;
import com.mygallo.entities.*;
import com.mygallo.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
/**
 * Created by jmorla on 16/08/17.
 */
@Controller
@ManagedBean
@ViewScoped
public class EditGalloController {

	@Autowired private GalloService galloService;
	@Autowired private ColorService colorService;
	@Autowired private TipoPlacaService tipoPlacaService;
	@Autowired private TipoCrestaService tipoCrestaService;
	@Autowired private EstadoService estadoService;
	@Autowired private MarcaService marcaService;
	@Autowired private EnraceService enraceService;

	private List<TipoPlaca> tipoPlacas;
	private List<Gallos> gallosMacho;
	private List<Gallos> gallosHembra;
	private List<Colores> colorAlas;
	private List<Marcas> marcas;
	private List<Enraces> enraces;
	private List<TipoCresta> tiposCresta;
	private List<Estados> estados;
	private String galloId;
	
	private boolean enable;
	private Usuarios usuario;
	private Gallos padre;
	private Gallos madre;
	private Estados estado;
	private TipoCresta tipoCresta;
	private Enraces enrace;
	private Marcas marca;
	private TipoPlaca tipoPlaca;
	private Colores color;
	private Gallos gallo;

	public void init(){

		usuario = (Usuarios) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
		System.out.println("Logged User: "+usuario.getIdUsuario());
		try {
			initNew();
		}catch (Exception e) {
			e.printStackTrace();
			try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("/secure/dashboard/");
			}catch(Exception ex) {
				//void
			}
		}
	}
	
	public void initNew() throws MyGalloException{
		
			gallo = galloService.findGalloByUserAndId(usuario.getIdUsuario(), galloId);
		
			tipoPlacas = tipoPlacaService.findAllTiposPlacas();
			gallosMacho = galloService.listGallosByUserSexoAndTipoPlaca(usuario.getIdUsuario(), "M", 1);
			gallosHembra = galloService.listGallosByUserSexoAndTipoPlaca(usuario.getIdUsuario(), "H", 1);
			colorAlas = colorService.findAllColores();
			tiposCresta = tipoCrestaService.findAllTipoCrestas();
			estados = estadoService.findEstados();
			marcas = marcaService.findAllMarcas();
			enraces = enraceService.finalAllEnraces();
			
			padre = gallo.getGallosByIdPadre()!=null?gallo.getGallosByIdPadre():new Gallos();
			madre = gallo.getGallosByIdMadre()!=null?gallo.getGallosByIdMadre():new Gallos();
			estado = gallo.getEstados()!=null ? gallo.getEstados() : new Estados();
			tipoCresta = gallo.getTipoCresta()!=null ? gallo.getTipoCresta() : new TipoCresta();
			enrace = gallo.getEnraces()!=null ? gallo.getEnraces() : new Enraces();
			marca = gallo.getMarcas() !=null ? gallo.getMarcas() : new Marcas();
			tipoPlaca = gallo.getTipoPlaca()!=null?gallo.getTipoPlaca():new TipoPlaca();
			padre.setTipoPlaca(gallo.getGallosByIdPadre()!=null?gallo.getGallosByIdPadre().getTipoPlaca():new TipoPlaca());
			madre.setTipoPlaca(gallo.getGallosByIdMadre()!=null?gallo.getGallosByIdMadre().getTipoPlaca():new TipoPlaca());
			color = gallo.getColores()!=null? gallo.getColores():new Colores();
			onFallecidoState();
			onChangeTipoPlacaMadre();
			onChangeTipoPlacaPadre();
		
	}

	public void onFallecidoState() {
		if (gallo.getEstados().getIdEstado() == 2) {
			enable = false;
		} else {
			enable = true;
			gallo.setFallecimiento(null);
		}
	}

	public String updateGallo() {
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage message = new FacesMessage();

		gallo.setTipoCresta(tipoCresta);
		gallo.setColores(color);
		gallo.setEnraces(enrace);
		gallo.setEstados(estado);
		gallo.setMarcas(marca);
		gallo.setTipoPlaca(tipoPlaca);

		if(!padre.getIdGallo().isEmpty()) {
			gallo.setGallosByIdPadre(padre);
		}
		if(!madre.getIdGallo().isEmpty()) {
			gallo.setGallosByIdMadre(madre);
		}
		try{
			galloService.updateGallo(gallo);

		}catch(InvalidNoPlacaException e){
			message.setSummary("El No. de placa "+gallo.getPlaca()+" ya fue asignado a otro perfil");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage("placa", message);

			return null;

		}catch(InvalidBetweenDateException e){
			message.setSummary(e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage("placa", message);
			return null;
		}catch (Exception e){
			e.printStackTrace();
		}
		return "/secure/index.xhtml?faces-redirect=true";
	}

	public String delete(){
        try {
            galloService.removeGallo(gallo);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Perfil eliminado con exito",null));
        }catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error al eliminar el perfil",null));
            return "";
        }
	    return "/secure/index.xhtml?faces-redirect=true";
    }

	public void onChangeTipoPlacaPadre() {
		gallosMacho = galloService.listGallosByUserSexoAndTipoPlaca(usuario.getIdUsuario(), "M",
				padre!=null? padre.getTipoPlaca().getIdTipoPlaca():0);

		for(int i=0;i<gallosMacho.size();i++){
			if(gallosMacho.get(i).getIdGallo().equals(galloId))
				gallosMacho.remove(i);
		}
	}

	public void onChangeTipoPlacaMadre() {
		gallosHembra = galloService.listGallosByUserSexoAndTipoPlaca(usuario.getIdUsuario(), "H",
			 madre!=null? madre.getTipoPlaca().getIdTipoPlaca():0);

		for(int i=0;i<gallosHembra.size();i++){
			if(gallosHembra.get(i).getIdGallo().equals(galloId))
				gallosHembra.remove(i);
		}
		
	}

	public Gallos getGallo() {
		return gallo;
	}

	public void setGallo(Gallos gallo) {
		this.gallo = gallo;
	}

	public List<TipoPlaca> getTipoPlacas() {
		return tipoPlacas;
	}

	public void setTipoPlacas(List<TipoPlaca> tipoPlacas) {
		this.tipoPlacas = tipoPlacas;
	}

	public List<Gallos> getGallosMacho() {
		return gallosMacho;
	}

	public void setGallosMacho(List<Gallos> gallosMacho) {
		this.gallosMacho = gallosMacho;
	}

	public List<Gallos> getGallosHembra() {
		return gallosHembra;
	}

	public void setGallosHembra(List<Gallos> gallosHembra) {
		this.gallosHembra = gallosHembra;
	}

	public List<Colores> getColorAlas() {
		return colorAlas;
	}

	public void setColorAlas(List<Colores> colorAlas) {
		this.colorAlas = colorAlas;
	}

	public List<Marcas> getMarcas() {
		return marcas;
	}

	public void setMarcas(List<Marcas> marcas) {
		this.marcas = marcas;
	}

	public List<Enraces> getEnraces() {
		return enraces;
	}

	public void setEnraces(List<Enraces> enraces) {
		this.enraces = enraces;
	}

	public List<TipoCresta> getTiposCresta() {
		return tiposCresta;
	}

	public void setTiposCresta(List<TipoCresta> tiposCresta) {
		this.tiposCresta = tiposCresta;
	}

	public List<Estados> getEstados() {
		return estados;
	}

	public void setEstados(List<Estados> estados) {
		this.estados = estados;
	}

	public String AddGallo() {
		return null;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public Usuarios getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}

	public Gallos getPadre() {
		return padre;
	}

	public void setPadre(Gallos padre) {
		this.padre = padre;
	}

	public Gallos getMadre() {
		return madre;
	}

	public void setMadre(Gallos madre) {
		this.madre = madre;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	public TipoCresta getTipoCresta() {
		return tipoCresta;
	}

	public void setTipoCresta(TipoCresta tipoCresta) {
		this.tipoCresta = tipoCresta;
	}

	public Enraces getEnrace() {
		return enrace;
	}

	public void setEnrace(Enraces enrace) {
		this.enrace = enrace;
	}

	public Marcas getMarca() {
		return marca;
	}

	public void setMarca(Marcas marca) {
		this.marca = marca;
	}

	public TipoPlaca getTipoPlaca() {
		return tipoPlaca;
	}

	public void setTipoPlaca(TipoPlaca tipoPlaca) {
		this.tipoPlaca = tipoPlaca;
	}

	public Colores getColor() {
		return color;
	}

	public void setColor(Colores color) {
		this.color = color;
	}

	public String getGalloId() {
		return galloId;
	}

	public void setGalloId(String galloId) {
		this.galloId = galloId;
	}

}
