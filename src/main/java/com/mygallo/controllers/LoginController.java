package com.mygallo.controllers;


import com.mygallo.entities.Usuarios;
import com.mygallo.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Created by jmorla on 05/07/17.
 */
@Controller
@Scope("session")
public class LoginController {

    @Autowired 
    private UsuarioService usuarioService;
    
    private Usuarios usuario;

    @Autowired 
    private AuthenticationManager authenticationManager;

    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }

    @PostConstruct
    public void init(){
        usuario = new Usuarios();
    }

    public String doLogin(){

        try{
            Authentication auth = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            usuario.getUsername(),usuario.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(auth);


            return "/secure/index.xhtml?faces-redirect=true";
        }catch (BadCredentialsException ex){
            FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,"Username or password incorrect",""));
        }

        return "";
    }

    public String doLogout(){
        SecurityContextHolder.clearContext();
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/login.xhtml?faces-redirect=true";
    }
}
