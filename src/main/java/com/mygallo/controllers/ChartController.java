package com.mygallo.controllers;

/**
 * Created by jmorla on 01/09/17.
 */
import java.util.LinkedHashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.mygallo.entities.Usuarios;
import com.mygallo.services.GalloService;
import org.primefaces.model.chart.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
@ManagedBean
@ViewScoped
public class ChartController {

    private DonutChartModel sexDonutModel;
    private LineChartModel bornPerMonthModel;
    private int max;

    @Autowired private GalloService galloService;

    public void init() {
        createChartsModels();
    }

    public DonutChartModel getSexDonutModel() {
        return sexDonutModel;
    }

    public LineChartModel getBornPerMonthModel(){
        return bornPerMonthModel;
    }

    private void createChartsModels() {
        Usuarios usuario = (Usuarios)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        long machos = (Long)galloService.countGallosBySexo(usuario.getIdUsuario(),"M");
        long hembras = (Long)galloService.countGallosBySexo(usuario.getIdUsuario(),"H");

        sexDonutModel = initDonutModel(machos,hembras);
        sexDonutModel.setTitle("Porcentage segun el sexo");
        sexDonutModel.setLegendPosition("e");
        sexDonutModel.setSliceMargin(5);
        sexDonutModel.setShowDataLabels(true);
        sexDonutModel.setDataFormat("value");
        sexDonutModel.setShadow(true);

        bornPerMonthModel = initBornPerMonthModel();
        bornPerMonthModel.setTitle("Nacimientos del ultimo año");
        bornPerMonthModel.setLegendPosition("e");
        bornPerMonthModel.setShowPointLabels(true);
        bornPerMonthModel.getAxes().put(AxisType.X, new CategoryAxis("Meses"));
        Axis yAxis = bornPerMonthModel.getAxis(AxisType.Y);
        yAxis.setLabel("Nacimientos");
        yAxis.setMin(0);
        yAxis.setMax(max);
        yAxis.setTickFormat("%.0f");

    }

    public LineChartModel initBornPerMonthModel(){
        int max = 0;
        LineChartModel model = new LineChartModel();
        Usuarios user = (Usuarios) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("user");

        int mData[] = galloService.groupBornsByMonth(user.getIdUsuario(),"M");
        for(int i = 0;i<mData.length;i++){
            max  = mData[i] > max?mData[i]:max;
        }
        LineChartSeries machos = new LineChartSeries();
        machos.setLabel("Machos");
        machos.set("Ene",mData[0]);
        machos.set("Feb",mData[1]);
        machos.set("Mar",mData[2]);
        machos.set("Abr",mData[3]);
        machos.set("May",mData[4]);
        machos.set("Jun",mData[5]);
        machos.set("Jul",mData[6]);
        machos.set("Ago",mData[7]);
        machos.set("Sep",mData[8]);
        machos.set("Oct",mData[9]);
        machos.set("Nov",mData[10]);
        machos.set("Dic",mData[11]);

        mData = galloService.groupBornsByMonth(user.getIdUsuario(),"H");
        for(int i = 0;i<mData.length;i++){
            max  = mData[i] > max?mData[i]:max;
        }
        LineChartSeries hembras = new LineChartSeries();
        hembras.setLabel("Hembras");
        hembras.set("Ene",mData[0]);
        hembras.set("Feb",mData[1]);
        hembras.set("Mar",mData[2]);
        hembras.set("Abr",mData[3]);
        hembras.set("May",mData[4]);
        hembras.set("Jun",mData[5]);
        hembras.set("Jul",mData[6]);
        hembras.set("Ago",mData[7]);
        hembras.set("Sep",mData[8]);
        hembras.set("Oct",mData[9]);
        hembras.set("Nov",mData[10]);
        hembras.set("Dic",mData[11]);

        model.addSeries(machos);
        model.addSeries(hembras);
        this.max = (max * 2);
        return model;
    }

    private DonutChartModel initDonutModel(long value1, long value2) {
        DonutChartModel model = new DonutChartModel();

        Map<String, Number> circle1 = new LinkedHashMap<String, Number>();
        circle1.put("Machos", value1);
        circle1.put("Hembras", value2);
        model.addCircle(circle1);

        return model;
    }
}
