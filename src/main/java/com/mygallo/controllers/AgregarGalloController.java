package com.mygallo.controllers;

import com.mygallo.common.exceptions.InvalidBetweenDateException;
import com.mygallo.common.exceptions.InvalidNoPlacaException;
import com.mygallo.entities.*;
import com.mygallo.services.*;
import com.mygallo.util.IdGenerator;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.List;
import javax.faces.application.FacesMessage;

@Controller
@ManagedBean
@ViewScoped
public class AgregarGalloController {

    @Autowired private GalloService galloService;
    @Autowired private ColorService colorService;
    @Autowired private TipoPlacaService tipoPlacaService;
    @Autowired private TipoCrestaService tipoCrestaService;
    @Autowired private EstadoService estadoService;
    @Autowired private MarcaService marcaService;
    @Autowired private EnraceService enraceService;

    private Gallos gallo;
    private List<TipoPlaca> tipoPlacas;
    private List<Gallos> gallosMacho;
    private List<Gallos> gallosHembra;
    private List<Colores> colorAlas;
    private List<Marcas> marcas;
    private List<Enraces> enraces;
    private List<TipoCresta> tiposCresta;
    private List<Estados> estados;
    private TipoCresta tipoCresta;
    private Colores color;
    private Marcas marca;
    private Enraces enrace;
    private TipoPlaca tipoPlaca;
    private Estados estado;
    private Gallos padre;
    private Gallos madre;
    private boolean enable;
    private Usuarios usuario;

    public void init(){
         usuario = (Usuarios) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("user");
        gallo = new Gallos();

        if(gallo.getIdGallo()==null){
            //gallo.setIdGallo(IdGenerator.getNextGalloId(galloService.findMaxGalloIdByUser()));
            gallo.setIdGallo(IdGenerator.getUuid());
        }else{
            gallo = galloService.findGalloByUserAndId(usuario.getIdUsuario(),gallo.getIdGallo());
        }
        gallo.setUsuarios(usuario);
        tipoPlacas = tipoPlacaService.findAllTiposPlacas();
//	for(int i=0;i<tipoPlacas.size();i++){
//		if(tipoPlacas.get(i).getIdTipoPlaca()==0){
//			remove.remove(i);
//		}
//	}
        gallosMacho = galloService.listGallosByUserSexoAndTipoPlaca(usuario.getIdUsuario(),"M",1);
        gallosHembra = galloService.listGallosByUserSexoAndTipoPlaca(usuario.getIdUsuario(),"H",1);
        colorAlas = colorService.findAllColores();
        tiposCresta = tipoCrestaService.findAllTipoCrestas();
        estados = estadoService.findEstados();
        marcas = marcaService.findAllMarcas();
        enraces = enraceService.finalAllEnraces();
        gallo.setTipoPlaca(new TipoPlaca());
        
        tipoCresta = new TipoCresta();
        color = new Colores();
        marca = new Marcas();
        enrace = new Enraces();
        tipoPlaca = new TipoPlaca();
        estado = new Estados();
        padre = new Gallos();
        madre = new Gallos();

        padre.setTipoPlaca(new TipoPlaca());
        madre.setTipoPlaca(new TipoPlaca());
        enable = true;

        onChangeTypePlaca();
        onChangeTipoPlacaMadre();
        onChangeTipoPlacaPadre();

    }
    public void onChangeTypePlaca(){
        int placa = tipoPlacaService.getMaxNoPlacaByTipoColor
                (tipoPlaca.getIdTipoPlaca());
        gallo.setPlaca(placa+1);
    }
    
    public void onFallecidoState(){
        if(estado.getIdEstado()==2){
            enable = false;
        }else{
            enable = true;
            gallo.setFallecimiento(null);
        }
    }

    public String deleteGallo(){
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message = new FacesMessage();

        try{
            galloService.removeGallo(gallo);
            return null;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "/secure/index.xhtml?faces-redirect=true";
    }
    public String addGallo(){
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message = new FacesMessage();

        gallo.setTipoCresta(tipoCresta);
        gallo.setColores(color);
        gallo.setEnraces(enrace);
        gallo.setEstados(estado);
        gallo.setMarcas(marca);
        gallo.setTipoPlaca(tipoPlaca);

        if(!padre.getIdGallo().isEmpty()) {
        	 gallo.setGallosByIdPadre(padre);
        }
        if(!madre.getIdGallo().isEmpty()) {
        	gallo.setGallosByIdMadre(madre);
        }

        try{
            galloService.guardarGallo(gallo);

        }catch(InvalidNoPlacaException e){

            message.setSummary("El No. de placa "+gallo.getPlaca()+" ya fue asignado a otro perfil");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage("placa", message);

            onChangeTypePlaca();

            return null;

        }catch(InvalidBetweenDateException e){

            message.setSummary(e.getMessage());
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage("placa", message);
            return null;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "/secure/index.xhtml?faces-redirect=true";
    }



    public void onChangeTipoPlacaPadre(){
        gallosMacho = galloService.listGallosByUserSexoAndTipoPlaca(usuario.getIdUsuario(),"M",
                padre.getTipoPlaca().getIdTipoPlaca());
    }

    public void onChangeTipoPlacaMadre(){
        gallosHembra = galloService.listGallosByUserSexoAndTipoPlaca(usuario.getIdUsuario(),"H",
                madre.getTipoPlaca().getIdTipoPlaca());
    }


    public Gallos getGallo() {
        return gallo;
    }

    public void setGallo(Gallos gallo) {
        this.gallo = gallo;
    }

    public List<TipoPlaca> getTipoPlacas() {
        return tipoPlacas;
    }

    public void setTipoPlacas(List<TipoPlaca> tipoPlacas) {
        this.tipoPlacas = tipoPlacas;
    }

    public List<Gallos> getGallosMacho() {
        return gallosMacho;
    }

    public void setGallosMacho(List<Gallos> gallosMacho) {
        this.gallosMacho = gallosMacho;
    }

    public List<Gallos> getGallosHembra() {
        return gallosHembra;
    }

    public void setGallosHembra(List<Gallos> gallosHembra) {
        this.gallosHembra = gallosHembra;
    }

    public List<Colores> getColorAlas() {
        return colorAlas;
    }

    public void setColorAlas(List<Colores> colorAlas) {
        this.colorAlas = colorAlas;
    }

    public List<Marcas> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Marcas> marcas) {
        this.marcas = marcas;
    }

    public List<Enraces> getEnraces() {
        return enraces;
    }

    public void setEnraces(List<Enraces> enraces) {
        this.enraces = enraces;
    }

    public List<TipoCresta> getTiposCresta() {
        return tiposCresta;
    }

    public void setTiposCresta(List<TipoCresta> tiposCresta) {
        this.tiposCresta = tiposCresta;
    }

    public List<Estados> getEstados() {
        return estados;
    }

    public void setEstados(List<Estados> estados) {
        this.estados = estados;
    }

    public String AddGallo(){
        return null;
    }

    public TipoCresta getTipoCresta() {
        return tipoCresta;
    }

    public void setTipoCresta(TipoCresta tipoCresta) {
        this.tipoCresta = tipoCresta;
    }

    public Colores getColor() {
        return color;
    }

    public void setColor(Colores color) {
        this.color = color;
    }

    public Marcas getMarca() {
        return marca;
    }

    public void setMarca(Marcas marca) {
        this.marca = marca;
    }

    public Enraces getEnrace() {
        return enrace;
    }

    public void setEnrace(Enraces enrace) {
        this.enrace = enrace;
    }

    public TipoPlaca getTipoPlaca() {
        return tipoPlaca;
    }

    public void setTipoPlaca(TipoPlaca tipoPlaca) {
        this.tipoPlaca = tipoPlaca;
    }

    public Estados getEstado() {
        return estado;
    }

    public void setEstado(Estados estado) {
        this.estado = estado;
    }

    public Gallos getPadre() {
        return padre;
    }

    public void setPadre(Gallos padre) {
        this.padre = padre;
    }

    public Gallos getMadre() {
        return madre;
    }

    public void setMadre(Gallos madre) {
        this.madre = madre;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
