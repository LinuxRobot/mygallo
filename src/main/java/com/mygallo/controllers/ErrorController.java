package com.mygallo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.*;
import javax.servlet.http.*;
import org.springframework.web.bind.annotation.*;


@Controller
public class ErrorController{

    @RequestMapping(value = "/404", method = RequestMethod.GET)
    public String error404(HttpServletRequest httpRequest) {
       
       return "/404.html";

    }
}