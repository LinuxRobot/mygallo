package com.mygallo.controllers;

import com.mygallo.entities.Usuarios;
import com.mygallo.repositories.UsuariosRepository;
import com.mygallo.services.UsuarioService;
import com.mygallo.util.ImageManager;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by jmorla on 05/07/17.
 */
@Controller
@Scope("session")
public class ProfileController {

    private Usuarios usuario;
    private UploadedFile photo;


    @Autowired private UsuarioService usuarioService;
    private ImageManager imageManager;
    private String password;

    public void updateProfile(){
        try{
            if(!password.isEmpty()){
                usuario.setPassword(password);
            }
            usuarioService.uploadProfile(usuario,photo);
            FacesContext.getCurrentInstance()
                    .addMessage(null,new FacesMessage
                            (FacesMessage.SEVERITY_INFO,"perfil actualizado",""));

        }catch (Exception ex){
            FacesContext.getCurrentInstance()
                    .addMessage(null,
                            new FacesMessage(
                                    FacesMessage.SEVERITY_FATAL,
                                    "no se pudo actualizar su perfil",""));
            ex.printStackTrace();
        }
    }

    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }

    public void init(){
        usuario = (Usuarios) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
    }

    public UploadedFile getPhoto() {
        return photo;
    }

    public void setPhoto(UploadedFile photo) {
        this.photo = photo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}