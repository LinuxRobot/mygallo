package com.mygallo.services;

import com.mygallo.common.exceptions.FileNotUploadException;
import com.mygallo.entities.Usuarios;
import com.mygallo.repositories.UsuariosRepository;
import com.mygallo.util.ImageManager;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.faces.context.FacesContext;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by jmorla on 09/07/17.
 */
@Service
public class UsuarioService implements UserDetailsService{

    @Autowired
    private UsuariosRepository usuariosRepository;

    public Usuarios findUserByUsername(String username){
        return usuariosRepository.findByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Usuarios usuario = findUserByUsername(s);

        if(!(usuario instanceof Usuarios)){
            throw new UsernameNotFoundException("User not found");
        }

        Collection<GrantedAuthority> authorities = new ArrayList(0);
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user",usuario);
        return new User(usuario.getUsername(),usuario.getPassword(),authorities);
    }

    @Transactional(rollbackFor = FileNotUploadException.class)
    public void uploadProfile(Usuarios u, UploadedFile photo) throws Exception {
        if(photo!=null){
            if(photo.getSize()>0){
                String oldphoto = u.getPhoto();
                ImageManager imageManager = new ImageManager();
                InputStream in = photo.getInputstream();
                File tempFile = File.createTempFile(photo.getFileName(), null);

                tempFile.deleteOnExit();

                FileOutputStream out = new FileOutputStream(tempFile);
                IOUtils.copy(in, out);
                imageManager = new ImageManager();
                try {
                    String url = imageManager.uploadImage(tempFile);
                    u.setPhoto(url);
                    usuariosRepository.save(u);
                }catch (Exception e){
                    throw new FileNotUploadException("Failed uploading file...");
                }finally {
                    deleteOldImage(oldphoto);
                }
            }
        }
    }

    public void deleteOldImage(String url){
        try {
            ImageManager imageManager = new ImageManager();
            imageManager.deleteImage(url);
        }catch (Exception e){
            //TODO
        }
    }
}
