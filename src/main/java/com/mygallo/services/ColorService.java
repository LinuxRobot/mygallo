package com.mygallo.services;

import com.mygallo.entities.Colores;
import com.mygallo.repositories.ColorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmorla on 13/07/17.
 */
@Service
public class ColorService {

    @Autowired
    private ColorRepository colorRepository;

    @Transactional(readOnly = true)
    public List<Colores> findAllColores(){
        return colorRepository.findAll();
    }
}
