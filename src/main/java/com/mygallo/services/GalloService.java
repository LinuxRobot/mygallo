package com.mygallo.services;

import com.mygallo.common.exceptions.InvalidBetweenDateException;
import com.mygallo.common.exceptions.InvalidNoPlacaException;
import com.mygallo.entities.Gallos;
import com.mygallo.repositories.GallosDAO;
import com.mygallo.repositories.GallosRepository;
import com.mygallo.repositories.PlacasDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

@Service
public class GalloService {

    @Autowired private GallosRepository gallosRepository;
    @Autowired private GallosDAO gallosDAO;
    @Autowired private PlacasDAO placasDAO;

    @Transactional(readOnly = true)
    public List<Gallos> findGallosByUser(String id){

        List<Gallos> lista = gallosRepository.findGallosByUserId(id);

        return lista;
    }

    @Transactional(readOnly = true)
    public List<Gallos> listadoGallosPorIdUsuarioYSexo(String id,String sexo){
        return gallosDAO.findGallosByUserIdAndSexo(id,sexo);
    }

    @Transactional(readOnly = true)
    public List<Gallos> listGallosByUserSexoAndTipoPlaca(String userid,String sexo,int idTipoPlaca){
        return gallosDAO.findGallosByUserSexoAndTipoPlaca(userid,sexo,idTipoPlaca);
    }

    @Transactional(rollbackFor = {InvalidNoPlacaException.class,InvalidBetweenDateException.class})
    public void updateGallo(Gallos gallo) throws InvalidNoPlacaException, InvalidBetweenDateException {
    	
        if(gallo.getFallecimiento()!=null){
            if(gallo.getNacimiento().getTime() > gallo.getFallecimiento().getTime()){
                throw new InvalidBetweenDateException("Fecha de nacimiento no puede ser mayor a la de fallecido");
            }
        }
        gallosRepository.save(gallo);
    }
    
    @Transactional(rollbackFor = {InvalidNoPlacaException.class,InvalidBetweenDateException.class})
    public void guardarGallo(Gallos gallo) throws InvalidNoPlacaException, InvalidBetweenDateException {

        boolean exist = placasDAO.existNoPlacaEnTipoPlaca(
                gallo.getTipoPlaca().getIdTipoPlaca(),gallo.getPlaca());

        if(exist){
            throw new InvalidNoPlacaException("Numero de placa ya existe en su tipo");
        }
        if(gallo.getFallecimiento()!=null){
            if(gallo.getNacimiento().getTime() > gallo.getFallecimiento().getTime()){
                throw new InvalidBetweenDateException("Fecha de nacimiento no puede ser mayor a la de fallecido");
            }
        }


        gallosRepository.save(gallo);
    }

    @Transactional
    public String findMaxGalloIdByUser(){
       
        return gallosDAO.maxGalloIdByUser();
       
    }
    
    @Transactional
    public void removeGallo(Gallos g) {
    	
    	List<Gallos> hijosFather = gallosDAO.findChildOfFatherByGallo(g);
    	List<Gallos> hijosMother = gallosDAO.findChildOfMotherByGallo(g);
    	System.out.println("I'm Here please look me right now!!! "+hijosMother.size());
    	
	    hijosFather.forEach((e)->{
	    	e.setGallosByIdPadre(null);
	    	
	    	try {
	    		System.out.println("1");
				this.updateGallo(e);
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
	    });
	    
	    hijosMother.forEach((e)->{
	    	e.setGallosByIdMadre(null);
	    	
	    	try {
	    		System.out.println("1");
				this.updateGallo(e);
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
	    });
	   
	    gallosRepository.delete(g);
    
    }

    @Transactional(readOnly = true)
    public Object countGallosBySexo(String userid,String sexo){
        return gallosDAO.cantGallosPorSexo(userid,sexo);
    }

    @Transactional(readOnly = true)
    public Object countGallosFallecidos(String userid){
        return gallosDAO.cantGallosFallecidos(userid);
    }

    @Transactional
    public Gallos findGalloByUserAndId(String userid,String galloid){
        return gallosDAO.findGallosByUserAndId(userid,galloid);
    }

    @Transactional
    public long countGallosByMonthSexAndUser(String userid,String month,String sex){
        BigInteger value = gallosDAO.countBornMonthAndSexByUser(userid,month,sex);
        return value.longValue();
    }

    @Transactional
    public int[] groupBornsByMonth(String userid,String sex){
        int monthBorn[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        List<Map<String,Integer>> result = gallosDAO.groupCountGalloBornByMonthSexAndUser(userid,sex);
        for(Map<String,Integer> key : result){
            int month = key.get("month");
            int cant = Integer.parseInt(key.get("cant")+"");

            monthBorn[month-1] = cant;
        }

        return monthBorn;
    }
   
}
