package com.mygallo.services;

import com.mygallo.entities.Estados;
import com.mygallo.repositories.EstadosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmorla on 13/07/17.
 */
@Service
public class EstadoService {


    @Autowired private EstadosRepository estadosRepository;

    @Transactional(readOnly = true)
    public List<Estados> findEstados(){
        return estadosRepository.findAll();
    }
}
