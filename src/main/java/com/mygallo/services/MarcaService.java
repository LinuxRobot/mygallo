package com.mygallo.services;

import com.mygallo.entities.Marcas;
import com.mygallo.repositories.MarcasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmorla on 20/07/17.
 */
@Service
public class MarcaService {

    @Autowired
    private MarcasRepository marcasRepository;

    @Transactional(readOnly = true)
    public List<Marcas> findAllMarcas(){
        return marcasRepository.findAll();
    }
}
