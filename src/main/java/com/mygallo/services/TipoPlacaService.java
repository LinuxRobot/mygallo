package com.mygallo.services;

import com.mygallo.entities.TipoPlaca;
import com.mygallo.repositories.PlacasDAO;
import com.mygallo.repositories.TipoPlacaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmorla on 16/07/17.
 */
@Service
public class TipoPlacaService {

    @Autowired private TipoPlacaRepository tipoPlacaRepository;

    @Autowired private PlacasDAO placasDAO;

    @Transactional(readOnly = true)
    public List<TipoPlaca> findAllTiposPlacas(){
        return tipoPlacaRepository.findAll();
    }

    @Transactional
    public List<String> findListPlacasColor(){
        List<String> list = new ArrayList<>(0);

        for (TipoPlaca element : findAllTiposPlacas()){
            list.add(element.getColor());
        }
        return list;
    }

    @Transactional(readOnly = true)
    public boolean contraintUniqueNoPlacaByTipoPlaca(int idtipo,int placa){
        return placasDAO.existNoPlacaEnTipoPlaca(idtipo,placa);
    }

    @Transactional
    public int getMaxNoPlacaByTipoColor(int idTipoPlaca){
        return placasDAO.getMaxPlacaByTipoPlaca(idTipoPlaca);
    }
   
    @Transactional
    public boolean placaIsAssigned(int idTipoPlaca,int placa){
        return placasDAO.existNoPlacaEnTipoPlaca(idTipoPlaca, placa);
    }
}
