package com.mygallo.services;

import com.mygallo.entities.Enraces;
import com.mygallo.repositories.EnracesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmorla on 20/07/17.
 */
@Service
public class EnraceService {

    @Autowired
    private EnracesRepository enracesRepository;

    @Transactional(readOnly = true)
    public List<Enraces> finalAllEnraces(){
      return enracesRepository.findAll();
    }
}
