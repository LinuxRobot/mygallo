package com.mygallo.services;

import com.mygallo.entities.TipoCresta;
import com.mygallo.repositories.CrestaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmorla on 13/07/17.
 */
@Service
public class TipoCrestaService {


    @Autowired
    private CrestaRepository crestaRepository;

    @Transactional(readOnly = true)
    public List<TipoCresta> findAllTipoCrestas(){
        return crestaRepository.findAll();
    }
}
