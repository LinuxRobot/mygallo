package com.mygallo.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by jmorla on 13/07/17.
 */
@Entity
@Table(name = "tipo_cresta"
        ,catalog = "mygallo"
,uniqueConstraints = @UniqueConstraint(columnNames = "tipo_cresta"))
public class TipoCresta implements Serializable{

    private int idTipoCresta;

    private String tipoCresta;

    private Set<Gallos> gallos = new HashSet<>(0);

    public TipoCresta(){}

    public TipoCresta(int idTipoCresta, String tipoCresta) {
        this.idTipoCresta = idTipoCresta;
        this.tipoCresta = tipoCresta;
    }

    public TipoCresta(int idTipoCresta, String tipoCresta, Set<Gallos> gallos) {
        this.idTipoCresta = idTipoCresta;
        this.tipoCresta = tipoCresta;
        this.gallos = gallos;
    }

    @Id
    @Column(name="id_tipo_cresta", unique=true, nullable=false)
    public int getIdTipoCresta() {
        return idTipoCresta;
    }


    public void setIdTipoCresta(int idTipoCresta) {
        this.idTipoCresta = idTipoCresta;
    }

    @Column(name="tipo_cresta", unique=true, nullable=false)
    public String getTipoCresta() {
        return tipoCresta;
    }

    public void setTipoCresta(String tipoCresta) {
        this.tipoCresta = tipoCresta;
    }

    @OneToMany(fetch=FetchType.LAZY, mappedBy="tipoCresta")
    public Set<Gallos> getGallos() {
        return gallos;
    }

    public void setGallos(Set<Gallos> gallos) {
        this.gallos = gallos;
    }
}
