package com.mygallo.entities;
// Generated 05/07/2017 11:04:49 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * EnracesRepository generated by hbm2java
 */
@Entity
@Table(name="enraces"
    ,catalog="mygallo"
    , uniqueConstraints = @UniqueConstraint(columnNames="enrace") 
)
public class Enraces  implements java.io.Serializable {


     private int idEnrace;
     private String enrace;
     private String comment;
     private Set<Gallos> galloses = new HashSet<Gallos>(0);

    public Enraces() {
    }

	
    public Enraces(int idEnrace, String enrace) {
        this.idEnrace = idEnrace;
        this.enrace = enrace;
    }
    public Enraces(int idEnrace, String enrace, String comment, Set<Gallos> galloses) {
       this.idEnrace = idEnrace;
       this.enrace = enrace;
       this.comment = comment;
       this.galloses = galloses;
    }
   
    @Id 
    @Column(name="id_enrace", unique=true, nullable=false)
    public int getIdEnrace() {
        return this.idEnrace;
    }
    
    public void setIdEnrace(int idEnrace) {
        this.idEnrace = idEnrace;
    }

    
    @Column(name="enrace", unique=true, nullable=false)
    public String getEnrace() {
        return this.enrace;
    }
    
    public void setEnrace(String enrace) {
        this.enrace = enrace;
    }

    
    @Column(name="comment")
    public String getComment() {
        return this.comment;
    }
    
    public void setComment(String comment) {
        this.comment = comment;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="enraces")
    public Set<Gallos> getGalloses() {
        return this.galloses;
    }
    
    public void setGalloses(Set<Gallos> galloses) {
        this.galloses = galloses;
    }




}


