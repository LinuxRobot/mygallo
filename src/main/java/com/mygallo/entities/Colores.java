package com.mygallo.entities;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by jmorla on 12/07/17.
 */
@Entity
@Table(name = "colores"
,catalog = "mygallo",
uniqueConstraints = @UniqueConstraint(columnNames = "color"))
public class Colores implements Serializable{

    private int idColor;
    private String color;
    private String hex;
    private Set<Gallos> gallos = new HashSet<>(0);

    public Colores(){

    }

    public Colores(int idColor, String color, String hex) {
        this.idColor = idColor;
        this.color = color;
        this.hex = hex;
    }

    public Colores(int idColor, String color, String hex, Set<Gallos> gallos) {
        this.idColor = idColor;
        this.color = color;
        this.hex = hex;
        this.gallos = gallos;
    }

    @Id
    @Column(name="id_color", unique=true, nullable=false)
    public int getIdColor() {
        return idColor;
    }


    public void setIdColor(int idColor) {
        this.idColor = idColor;
    }

    @Column(name="color", unique=true, nullable=false)
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Column(name="hex")
    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    @OneToMany(fetch=FetchType.LAZY, mappedBy="colores")
    public Set<Gallos> getGallos() {
        return gallos;
    }

    public void setGallos(Set<Gallos> gallos) {
        this.gallos = gallos;
    }
}
