package com.mygallo.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by jmorla on 16/07/17.
 */
@Entity
@Table(name="tipo_placa"
        ,catalog="mygallo"
        , uniqueConstraints = @UniqueConstraint(columnNames="color")
)
public class TipoPlaca {

    private int idTipoPlaca;
    private String color;
    private String comment;
    private String hex;
    private Set<Gallos> gallosList = new HashSet<>(0);

    @Id
    @Column(name="id_tipo_placa",unique = true,nullable = false)
    public int getIdTipoPlaca() {
        return idTipoPlaca;
    }

    public void setIdTipoPlaca(int idTipoPlaca) {
        this.idTipoPlaca = idTipoPlaca;
    }

    @Column(name="color",unique = true,nullable = false)
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "tipoPlaca")
    public Set<Gallos> getGallosList() {
        return gallosList;
    }

    public void setGallosList(Set<Gallos> gallosList) {
        this.gallosList = gallosList;
    }

    @Column(name = "hex")
    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }
}
