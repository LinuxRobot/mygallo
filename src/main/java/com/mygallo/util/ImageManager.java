package com.mygallo.util;

import com.cloudinary.*;
import com.cloudinary.utils.ObjectUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;

public class ImageManager {

    private Cloudinary cloudinary;
    final private String CLOUDNAME = "gallerin-dominicana";
    final private String APIKEY = "721445268886593";
    final private String APISECRET = "CBdd3NxmRniL3kdfEHnz5sen3GI";

    public ImageManager() {
        Map config = ObjectUtils.asMap(
                "cloud_name", CLOUDNAME,
                "api_key", APIKEY,
                "api_secret", APISECRET);
        cloudinary = new Cloudinary(config);
    }

    public String uploadImage(File file) throws IOException {
        return cloudinary.uploader().upload(file,ObjectUtils.emptyMap()).get("url").toString();
    }

    public void deleteImage(String url){
        try {
            String key = getKeyFromUrl(url);
            cloudinary.uploader().destroy(key, ObjectUtils.emptyMap());
        }catch (Exception e){
            System.err.println("imagen no existe");
        }
    }

    private String getKeyFromUrl(String url){
        if(!url.isEmpty()){
            if(url.startsWith("http://")){
                String elements[] = url.split("/");
                String imageName = elements[elements.length-1];
                return imageName.split(Pattern.quote("."))[0];

            }
        }
        return null;
    }

    public static void main(String[] args) {
        ImageManager imageManager = new ImageManager();
        String key = imageManager.getKeyFromUrl("http://res.cloudinary.com/demo/image/upload/v1375302801/tquyfignx5bxcbsupr6a.jpg");
        System.out.println(key);
    }
}
