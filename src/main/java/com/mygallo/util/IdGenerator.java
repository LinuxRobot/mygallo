package com.mygallo.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by jmorla on 28/07/17.
 */
public class IdGenerator {

    public static String hashMD5(String field){
        try {
            byte[] data = field.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("md5");
            md.update(field.getBytes());

            byte digest[] = md.digest();
            StringBuffer buffer = new StringBuffer();
            for (byte b : digest) {
                buffer.append(String.format("%02x", b & 0xff));
            }

            return buffer.toString();

        }catch (UnsupportedEncodingException | NoSuchAlgorithmException ex){
            ex.printStackTrace();
        }

        return null;

    }

    public static String getUuid(){
        return java.util.UUID.randomUUID().toString();
    }

    public static String getNextGalloId(String last){
        String numero = last.substring(4,last.length());
        String prefix = last.substring(0,4);

        int id = Integer.parseInt(numero);
        id ++;
        numero = id+"";
        int i = 4 - numero.length();
        while(i>0){
            numero = "0"+numero;
            i--;
        }
        numero = prefix + numero;

        return numero;
    }

    public static void main(String[] args) {
        System.out.println(IdGenerator.getNextGalloId("JLMR3421"));
    }


}
