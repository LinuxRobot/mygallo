package com.mygallo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by jmorla on 05/07/17.
 */
@Configuration
public class MyGalloConfigure extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/mygallo-1.0/").setViewName("/mygallo-1.0/login/");
    }


}
