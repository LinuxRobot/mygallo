package com.mygallo.common.exceptions;

/**
 * Created by jmorla on 26/07/17.
 */
public class InvalidNoPlacaException extends MyGalloException {

    public InvalidNoPlacaException(String message){
        super(message);
    }
}
