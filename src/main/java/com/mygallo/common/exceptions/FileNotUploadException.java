package com.mygallo.common.exceptions;

/**
 * Created by jmorla on 01/09/17.
 */
public class FileNotUploadException extends MyGalloException {

    public FileNotUploadException(){
        super();
    }

    public FileNotUploadException(String message){
        super(message);
    }
}
