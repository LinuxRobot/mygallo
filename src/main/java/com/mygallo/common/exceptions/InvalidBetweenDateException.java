
package com.mygallo.common.exceptions;

/**
 *
 * @author jmorla
 */
public class InvalidBetweenDateException extends MyGalloException{
    
    public InvalidBetweenDateException(String message){
        super(message);
    }
}
