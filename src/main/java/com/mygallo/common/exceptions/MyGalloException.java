package com.mygallo.common.exceptions;

public class MyGalloException extends Exception {

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return super.getMessage();
	}

	@Override
	public synchronized Throwable getCause() {
		// TODO Auto-generated method stub
		return super.getCause();
	}

	public MyGalloException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MyGalloException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public MyGalloException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MyGalloException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MyGalloException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
