package com.mygallo.repositories;

import com.mygallo.entities.Estados;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jmorla on 05/07/17.
 */
@Repository
public interface EstadosRepository extends CrudRepository<Estados,Integer> {

    List<Estados> findAll();
}
