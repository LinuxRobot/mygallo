package com.mygallo.repositories;

import com.mygallo.entities.Gallos;
import com.mygallo.entities.Marcas;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import com.mygallo.entities.Usuarios;
/**
 * Created by jmorla on 05/07/17.
 */
@Repository
public interface GallosRepository extends CrudRepository<Gallos,String> {

	@Query("select g from Gallos g where g.usuarios.idUsuario = :id")
	List<Gallos> findGallosByUserId(@Param("id") String id);

	@Override
	void delete(Gallos gallo);

}
