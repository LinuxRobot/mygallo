package com.mygallo.repositories;

import com.mygallo.entities.Gallos;
import com.mygallo.entities.Usuarios;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by jmorla on 20/07/17.
 */
@Repository
public class GallosDAO {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    public List<Gallos> findGallosByUserIdAndSexo(String id,String sexo){
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);

        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Gallos.class);
        criteria.createAlias("tipoPlaca","tp")
        .add(Restrictions.eq("sexo",sexo))
        .createAlias("usuarios","u").add(Restrictions.eq("u.idUsuario",id));

        return criteria.list();
    }

    public List<Gallos> findGallosByUserSexoAndTipoPlaca(String userid, String sexo, int idTipoPlaca){
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);

        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Gallos.class);
        criteria.createAlias("tipoPlaca","tp")
                .add(Restrictions.eq("sexo",sexo))
                .createAlias("usuarios","u").add(Restrictions.eq("u.idUsuario",userid))
        .add(Restrictions.eq("tp.idTipoPlaca",idTipoPlaca));

        return criteria.list();
    }

    public String maxGalloIdByUser(){
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Usuarios usuarios = (Usuarios) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");

        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Gallos.class);
        criteria.add(Restrictions.like("idGallo",usuarios.getPrefix()+"%"))
                .addOrder(Order.desc("idGallo")).setMaxResults(1);
        if(criteria.uniqueResult()==null){
            return usuarios.getPrefix()+"0000";
        }

        return ((Gallos)criteria.uniqueResult()).getIdGallo();
    }

    public List<Gallos> findGallosByUserId(String userid){
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);

        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Gallos.class);
        criteria.createAlias("estados","es")
                .createAlias("gallosByIdPadre","padre")
                .createAlias("gallosByIdMadre","madre")
                .createAlias("marcas","m")
                .createAlias("colores","c")
                .createAlias("tipoCresta","tc")
                .createAlias("tipoPlaca","tp")
                .createAlias("usuarios","u");
        criteria.add(Restrictions.eq("u.idUsuario",userid));

        return criteria.list();
    }

    public Object cantGallosPorSexo(String userid,String sexo){
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Gallos.class);
        criteria.createAlias("usuarios","u");
        criteria.createAlias("estados","e");
        criteria.add(Restrictions.ne("e.idEstado",2));
        criteria.add(Restrictions.eq("sexo",sexo));
        criteria.add(Restrictions.eq("u.idUsuario",userid));
        criteria.setProjection(Projections.count("idGallo"));

        return criteria.uniqueResult();
    }

    public Object cantGallosFallecidos(String userid){
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Gallos.class);
        criteria.createAlias("usuarios","u");
        criteria.createAlias("estados","e");
        criteria.add(Restrictions.eq("e.idEstado",2));
        criteria.add(Restrictions.eq("u.idUsuario",userid));
        criteria.setProjection(Projections.count("idGallo"));

        return  criteria.uniqueResult();
    }

    public Gallos findGallosByUserAndId(String userid,String galloid){
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Gallos.class);
        criteria.createAlias("usuarios","u");
        criteria.add(Restrictions.eq("u.idUsuario",userid))
                .add(Restrictions.eq("idGallo",galloid));

        return (Gallos)criteria.uniqueResult();
    }
    
    public List<Gallos> findChildOfFatherByGallo(Gallos gallo){
    	SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Gallos.class);
    	
    	criteria.add(Restrictions.eq("padre.idGallo", gallo.getIdGallo()));
    	criteria.createAlias("gallosByIdPadre", "padre");
    	
    	return criteria.list();
    }
    
    public List<Gallos> findChildOfMotherByGallo(Gallos gallo){
    	SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
    	Session session = sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(Gallos.class);
    	
    	criteria.add(Restrictions.eq("madre.idGallo", gallo.getIdGallo()));
    	criteria.createAlias("gallosByIdMadre", "madre");
    	
    	return criteria.list();
    }
    
    public BigInteger countBornMonthAndSexByUser(String userid, String month, String sex){
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);

        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Session session = sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery("select COUNT(*)" +
                " from gallos g " +
                "WHERE nacimiento " +
                "like :fecha AND id_usuario = :userid AND sexo = :sexo");
        query.setParameter("fecha",year+"-"+month+"%");
        query.setParameter("userid",userid);
        query.setParameter("sexo",sex);

        return (BigInteger) query.uniqueResult();
    }

    public List<Map<String,Integer>> groupCountGalloBornByMonthSexAndUser(String userid,String sex){
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);

        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Session session= sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery("" +
                "select MONTH(nacimiento) as month,count(*) as cant" +
                " from gallos " +
                "where id_usuario =:usuario and sexo =:sexo and YEAR(nacimiento) =:ano " +
                "group by month; ");
        query.setParameter("usuario",userid);
        query.setParameter("sexo",sex);
        query.setParameter("ano",year);

        query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
        List<Map<String,Integer>> result = query.list();

        return result;
    }
}
