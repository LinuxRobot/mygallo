package com.mygallo.repositories;

import com.mygallo.entities.Colores;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by jmorla on 13/07/17.
 */
public interface ColorRepository  extends CrudRepository<Colores,Integer>{

    List<Colores> findAll();
}
