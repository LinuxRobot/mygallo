package com.mygallo.repositories;

import com.mygallo.entities.Usuarios;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jmorla on 05/07/17.
 */
@Repository
public interface UsuariosRepository extends CrudRepository<Usuarios,String> {

    public Usuarios findByUsernameAndPassword(String username,String password);

    public Usuarios findByUsername(String username);
}
