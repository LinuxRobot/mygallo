package com.mygallo.repositories;

import com.mygallo.entities.Gallos;
import com.mygallo.entities.TipoPlaca;
import com.mygallo.entities.Usuarios;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * Created by jmorla on 23/07/17.
 */
@Repository
public class PlacasDAO {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    public boolean existNoPlacaEnTipoPlaca(int idTipoPlaca,int placa){
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);

        String idUsuario = ((Usuarios)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user")).getIdUsuario();
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Gallos.class);
        criteria.add(Restrictions.eq("placa",placa))
                .createAlias("tipoPlaca","tp")
                .createAlias("usuarios","u")
                .add(Restrictions.eq("u.idUsuario",idUsuario))
                .add(Restrictions.eq("tp.idTipoPlaca",idTipoPlaca));

        return criteria.uniqueResult()!=null;
    }

    public int getMaxPlacaByTipoPlaca(int idTipoPlaca){
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        String idUsuario = ((Usuarios)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user")).getIdUsuario();
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Gallos.class);
        criteria.setProjection(Projections.max("placa"))
                .createAlias("tipoPlaca","tp")
                .createAlias("usuarios","u")
                .add(Restrictions.eq("u.idUsuario",idUsuario))
                .add(Restrictions.eq("tp.idTipoPlaca",idTipoPlaca));

        return (Integer) criteria.uniqueResult()!=null?(Integer)criteria.uniqueResult():0;
    }
}
