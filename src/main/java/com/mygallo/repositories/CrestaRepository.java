package com.mygallo.repositories;

import com.mygallo.entities.TipoCresta;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by jmorla on 13/07/17.
 */
public interface CrestaRepository extends CrudRepository<TipoCresta,Integer>{

    List<TipoCresta> findAll();
}
