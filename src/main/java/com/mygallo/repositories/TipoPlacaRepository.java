package com.mygallo.repositories;

import com.mygallo.entities.TipoPlaca;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by jmorla on 16/07/17.
 */
public interface TipoPlacaRepository extends CrudRepository<TipoPlaca,Integer> {

    List<TipoPlaca> findAll();
}
