package com.mygallo.repositories;

import com.mygallo.entities.Enraces;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jmorla on 05/07/17.
 */
@Repository
public interface EnracesRepository extends CrudRepository<Enraces,Integer>{

    public List<Enraces> findAll();
}
