package com.mygallo.repositories;

import com.mygallo.entities.Marcas;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jmorla on 05/07/17.
 */
@Repository
public interface MarcasRepository extends CrudRepository<Marcas,Integer> {

    List<Marcas> findAll();
}
