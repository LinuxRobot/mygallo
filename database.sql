
create database mygallo;
use mygallo;

create table usuarios(
	id_usuario varchar(255) primary key,
	username varchar(30) not null unique,
	password varchar(255) not null,
	nombre varchar(70) not null,
	apellido varchar(70) not null,
	correo varchar(155) not null unique,
	prefix varchar(6) not null unique,
	photo varchar(255) not null
);

create table estados(
	id_estado integer primary key,
	estado varchar(60) not null unique,
	comment varchar(255)
);

create table enraces(
	id_enrace integer primary key,
	enrace varchar(255) not null unique,
	comment varchar(255)
);

create table tipo_placa(
	id_tipo_placa integer PRIMARY KEY,
	color VARCHAR(40) not null UNIQUE,
  hex varchar(20),
	comment VARCHAR(255)
);

create table colores(
	id_color integer PRIMARY KEY,
	color VARCHAR(50) not null unique,
	hex VARCHAR(20)

);

create table marcas(
	id_marca integer primary key,
	marca varchar(255) not null unique,
	comment varchar(255)
);


CREATE TABLE tipo_cresta(
  id_tipo_cresta INTEGER PRIMARY KEY,
  tipo_cresta VARCHAR(100) NOT NULL  UNIQUE
);

create table gallos(
	id_gallo varchar(255) primary key,
	nacimiento timestamp not null,
  alias VARCHAR(100),
  placa integer not null,
	id_marca integer not null,
	fallecimiento timestamp,
	id_usuario varchar(255),
	id_padre varchar(255),
	id_madre varchar(255),
	sexo varchar(10) not null,
	id_estado integer not null,
	id_enrace integer not null,
	comment varchar(1024),
	id_color integer not null,
  id_tipo_cresta integer not null,
	id_tipo_placa integer not null,

	constraint fk_marca_gallo foreign key(id_marca)
	references marcas(id_marca),

	constraint fk_usuario_gallo foreign key(id_usuario)
	references usuarios(id_usuario),

	constraint fk_gallos_padre foreign key(id_padre)
	references gallos(id_gallo),

	constraint fk_gallos_madre foreign key(id_madre)
	references gallos(id_gallo),

	constraint fk_estado_gallo foreign key(id_estado)
	references estados(id_estado),

	constraint fk_enrace_gallo foreign key(id_enrace)
	references enraces(id_enrace),

	CONSTRAINT fk_color_gallo FOREIGN KEY (id_color)
	REFERENCES colores(id_color),

  CONSTRAINT fk_tipo_cresta_gallo FOREIGN KEY (id_tipo_cresta)
  REFERENCES tipo_cresta(id_tipo_cresta),

	CONSTRAINT fk_tipo_placa_gallo FOREIGN KEY (id_tipo_placa)
	REFERENCES tipo_placa(id_tipo_placa)
);


insert into tipo_cresta values(1,'none');
insert into tipo_cresta values(2,'Creston peineta');
insert into tipo_cresta values(3,'Roson');
insert into tipo_cresta values(4,'Pava');
insert into tipo_cresta values(5,'Pava Roson');

insert into colores VALUES (1,'Indio',null);
insert into colores VALUES (2,'Joco',null);
insert into colores VALUES (3,'Joco prieto',null);
insert into colores VALUES (4,'Canelo',null);
insert into colores VALUES (5,'Pinto',null);
insert into colores VALUES (6,'Jabao Avispa',null);
insert into colores VALUES (7,'Jabao Blanco',null);
insert into colores VALUES (8,'Giro Dorado',null);
insert into colores VALUES (9,'Giro Negro',null);
insert into colores VALUES (10,'Negro',null);
insert into colores VALUES (11,'Blanco',null);
insert into colores VALUES (12,'Cenizo Indio',null);
insert into colores VALUES (13,'Cenizo Amarillo',null);
insert into colores VALUES (14,'Cenizo Pizzara',null);

insert into enraces values(1,'NINGUNO','none');
insert into marcas values(2,'N1','');
insert into marcas values(3,'N1.N2','');
insert into marcas values(4,'N2','');
insert into marcas values(5,'P1','');
insert into marcas values(6,'P1.2','');
insert into marcas values(7,'P1.2.3','');
insert into marcas values(8,'P1.2.3.N1','');
insert into marcas values(9,'P1.2.3.N2','');
insert into marcas values(10,'P1.2.4','');
insert into marcas values(11,'P1.2.4.N1','');
insert into marcas values(12,'P1.2.4.N2','');
insert into marcas values(13,'P1.2.N1','');
insert into marcas values(14,'P1.2.N2','');
insert into marcas values(15,'P1.3','');
insert into marcas values(16,'P1.3.N1','');
insert into marcas values(17,'P1.3.N2','');
insert into marcas values(18,'P1.4','');
insert into marcas values(19,'P1.4.N1','');
insert into marcas values(20,'P1.4.N2','');
insert into marcas values(21,'P1.N1','');
insert into marcas values(22,'P1.N1.N2','');
insert into marcas values(23,'P1.N2','');
insert into marcas values(24,'P2','');
insert into marcas values(25,'P2.3','');
insert into marcas values(26,'P2.3.4','');
insert into marcas values(27,'P2.3.N1','');
insert into marcas values(28,'P2.3.N2','');
insert into marcas values(29,'P2.4','');
insert into marcas values(30,'P2.4.N1','');
insert into marcas values(31,'P2.4.N2','');
insert into marcas values(32,'P2.N1','');
insert into marcas values(33,'P2.N1.N2','');
insert into marcas values(34,'P2.N2','');
insert into marcas values(35,'P2.N2.N2','');
insert into marcas values(36,'P3','');;
insert into marcas values(37,'P3.4.N1','');
insert into marcas values(38,'P3.4.N2','');
insert into marcas values(39,'P3.N1','');
insert into marcas values(40,'P3.N1.N2','');
insert into marcas values(41,'P3.N2','');
insert into marcas values(42,'P4','');
insert into marcas values(43,'P4.N1','');
insert into marcas values(44,'P4.N1.N2','');
insert into marcas values(45,'P4.N2','');

insert into estados values(1,'VIVO','');
insert into estados values(2,'FALLECIDO','');
insert into estados values(3,'REGALADO','');
insert into estados values(4,'VENDIDO','');
insert into estados values(5,'OTRO','');

insert into tipo_placa VALUES (1,'Amarillo','yellow','');
insert into tipo_placa VALUES (2,'Naranga','orange','');
insert into tipo_placa VALUES (3,'Rojo','red','');
insert into tipo_placa VALUES (4,'Morado','purple','');
insert into tipo_placa VALUES (5,'Azul','blue','');
insert into tipo_placa VALUES (6,'Verde','green','');
insert into tipo_placa VALUES (7,'Negro','black','');
insert into tipo_placa VALUES (8,'Blanco','white','');
insert into tipo_placa VALUES (9,'Plateado','silver','');
insert into tipo_placa VALUES (10,'Rosa','pink','');

insert into usuarios values('61c37893-e229-4360-9246-ee3b0e43b719','jorge','12345','Jorge Luis','Morla Reyes','jorgemorlapro@gmail.com','JLMR','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQzoL7LYphmQJUakxuGQMoTicLEMGTdb9fhBXoabCzfW44aNaZEA');
insert into usuarios values('a52d0297-20a7-4d12-8c99-e02b87cc1900','admin','admin','armando gonzalez','pena dominguez','armandopena@hotmail.com','HMUS','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQzoL7LYphmQJUakxuGQMoTicLEMGTdb9fhBXoabCzfW44aNaZEA');

-- MR
insert into gallos values ('HMUS0001',sysdate(),'super gallo',100,12,null,'a52d0297-20a7-4d12-8c99-e02b87cc1900',null,null,'M',2,1,'grand father',1,1,10);
insert into gallos values ('HMUS0002',sysdate(),'',101,12,null,'a52d0297-20a7-4d12-8c99-e02b87cc1900',null,null,'H',2,1,'grand mother',2,2,9);

insert into gallos values ('JLMR0001',sysdate(),'',100,12,null,'61c37893-e229-4360-9246-ee3b0e43b719',null,null,'M',3,1,'grand father',2,3,8);
insert into gallos values ('JLMR0002',sysdate(),'',101,12,null,'61c37893-e229-4360-9246-ee3b0e43b719',null,null,'H',3,1,'grand mother',3,4,8);

create user admin identified by 'admin'
grant all privileges on mygallo.* to admin
flush privileges


