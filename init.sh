#!/bin/sh

USERNAME='root'
DATABASE='mygallo'
USERPASS='root'

clear
echo "================================================================================="
echo "*********************************************************************************"
echo "******************************INITIALIZER PROYECT********************************"
echo "*********************************************************************************"
echo "================================================================================="

mysql -u root -p < database.sql

read -p "root password: " USERPASS
echo $USERPASS

echo "configurando conexion a base de datos del proyecto..."
oldpassword=`grep -i 'spring.datasource.password' src/main/resources/application.properties`
sed -i "s/$oldpassword/spring.datasource.password = $USERPASS/g" src/main/resources/application.properties

echo "aplicacion configurada"
