#!/bin/bash

PACKAGE_NAME=`grep "<artifactId>" pom.xml | head -1 | cut -d'>' -f2 | cut -d'<' -f1`
PACKAGE_TYPE=war

CATALINA_HOME=/usr/local/apache-tomcat-8.0.27
VERSION=`grep "<version>" pom.xml | head -1 | cut -d'>' -f2 | cut -d'<' -f1`
if [ "$1" == "-a" ]; then
	case "$2" in
		DEPLOY)
		echo "shutting down the SERVER" 
		$CATALINA_HOME/bin/./shutdown.sh
		echo "BUILDING AND PACKAGING PROYECT"
		mvn clean
		mvn package
		
		echo "DROP OLD APPLICATION"
		rm -rf $CATALINA_HOME/webapps/ROOT/*
		
		echo "COPYING IN THE SERVER"
		cp target/mygallo-1.0.war $CATALINA_HOME/webapps/ROOT/mygallo.war
		
		echo "UNCOMPRESS"
		cd $CATALINA_HOME/webapps/ROOT/
		
		jar -xvf $CATALINA_HOME/webapps/ROOT/mygallo.war
		rm -rf $CATALINA_HOME/webapps/ROOT/mygallo.war
		
		cd ~/proyect/Resources/mygallo/

		$CATALINA_HOME/bin/./startup.sh
		echo "startup server"
		;;
		STOP)
		echo "shutdown server"
		$CATALINA_HOME/bin/./shutdown.sh
		;;
		START)
		echo "start server"
		$CATALINA_HOME/bin/./startup.sh	
	esac

else
	echo "-a RESTART|STOP|DEPLOY"
fi
